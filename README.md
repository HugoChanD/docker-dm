# README #

达梦数据库v8容器

### 下载编译 ###

* git clone https://HugoChanD@bitbucket.org/HugoChanD/docker-dm.git
* cd docker-dm
* docker build -t hugochan/dm:latest .

### docker启动 ###

* docker run -v /dm:/home/dmdba/data -p 5236:5236 -d --restart always hugochan/dm:latest

### docker-compose 启动 ###

* docker-compose up -d

### 使用 ###

* 默认账号密码:SYSDBA/SYSDBA`